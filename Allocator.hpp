// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <cmath>     // abs
#include <iostream> // cout

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& left, const iterator& right) {
            // <your code>
            return left._p == right._p;
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        //p is current address of iterator?
        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int* operator * () const {
            // <your code>
            return _p;
        }           // replace!

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            // <your code>
            //sentinel value
            int cur = *_p;
            cur = std::abs(cur);
            char* move = (char*) _p;
            move += cur + 8;
            _p = (int*) move;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            // <your code>
            int* temp = _p;
            // get value of sentinel from block immediately before
            --temp;
            int cur = *temp;
            cur = std::abs(cur);
            char* move = (char*) _p;
            move -= (8 + cur);
            _p = (int*) move;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& left, const const_iterator& right) {
            // <your code>
            return left._p == right._p;
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int* operator * () const {
            // <your code>
            return _p;
        }                 // replace!

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            // <your code>
            int cur = *_p;
            cur = std::abs(cur);
            char* move = (char*) _p;
            move += cur + 8;
            _p = (int*) move;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            // <your code>
            int* temp = _p;
            // get value of sentinel from block immediately before
            --temp;
            int cur = *temp;
            cur = std::abs(cur);
            char* move = (char*) _p;
            move -= (8 + cur);
            _p -= (int*) move;
            return *this;
        }
        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * iterates through all blocks, making sure it adds up to 1000 bytes
     * makes sure there are no adjacent free blocks (should be coalesced)
     */
    bool valid () const {
        // <your code>
        // <use iterators>
        int sum = 0;
        bool prevFree = false;
        const_iterator b = begin();
        const_iterator e = end();
        while(b != e) {
            // two adjacent free blocks
            if(prevFree && **b > 0) {
                std::cout << "two free blocks in a row";
                std::cout.flush();
                return false;
            }

            sum += 8 + std::abs(**b);
            if(**b > 0)
                prevFree = true;
            else
                prevFree = false;
            ++b;
        }
        if(sum != N) {
            std::cout << "total does not add up to " << N << "\n sum is: " << sum;
            std::cout.flush();
            return false;
        }
        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if(N < sizeof(T) + 8)
            throw std::bad_alloc();

        int initialSpace = N - 8;
        //first sentinel value
        int* left = (int*) a; // replace!
        *left = initialSpace;
        // <your code>
        //second sentinel value
        char* temp = a;
        temp += initialSpace + 4;
        int* right = (int*) temp;
        *right = initialSpace;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    void print_blocks() {
        iterator b = begin();
        iterator e = end();
        if(b != e) {
            std::cout << **b;
            ++b;
        }
        while(b != e) {
            int cur = **b;
            std::cout << " " << cur;
            ++b;
        }
        std::cout << std::endl;
    }

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        // <your code>
        if(n <= 0)
            throw std::bad_alloc();
        //first fit algorithm
        iterator b = begin();
        iterator e = end();
        int sizeNeeded = n * sizeof(T);
        int minBlock = sizeof(T) + 8;

        while(b != e) {
            int* temp = *b;
            int* beginBusy = ++temp;
            --temp;
            char* move = (char*) temp;
            int curSize = *temp;

            // can use current block, split into busy + free block afterwards
            if(curSize >= sizeNeeded + minBlock) {

                int busySize = -1 * sizeNeeded;
                //setting 1st busy sentinel
                *temp = busySize;
                //moving to 2nd busy sentinel and setting it
                move += std::abs(busySize) + 4;
                temp = (int*) move;
                *temp = busySize;

                int freeSize = curSize + busySize - 8;
                //moving to 1st free sentinel and setting it
                move = (char*) temp;
                move += 4;
                temp = (int*) move;
                *temp = freeSize;
                move = (char*) temp;
                move += freeSize + 4;
                temp = (int*) move;
                *temp = freeSize;
                return (pointer) beginBusy;
            }

            // can use current block, all of it becomes busy
            if(curSize >= sizeNeeded) {
                //set 1st sentinel
                *temp = -1 * curSize;
                //move to and set second sentinel
                move += curSize + 4;
                temp = (int*) move;
                *temp = -1 * curSize;
                return (pointer) beginBusy;
            }
            //not enough space to allocate in current block
            ++b;
        }
        throw std::bad_alloc();
    }             // replace!

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type) {
        // <your code>
        int* temp = (int*) p;
        // if(sentinel > 0) {
        //     throw std::invalid_argument("This is a free block");
        // }
        // if(sentinel/sizeof(T) < n) {
        //     throw std::invalid_argument("Trying to free more than size of pointed to block");
        // }
        iterator b = begin();
        iterator e = end();
        iterator currentBlockIter = iterator(temp);
        int* left = NULL;
        int* right = NULL;
        bool leftFree = false;
        bool rightFree = false;
        // storing left and right neighbhors
        if(currentBlockIter != b) {
            left = *--currentBlockIter;
            *left = **currentBlockIter;
            ++currentBlockIter;
            if(*left > 0)
                leftFree = true;
        }
        if(currentBlockIter != --e) {
            right = *++currentBlockIter;
            *right = **currentBlockIter;
            --currentBlockIter;
            if(*right > 0)
                rightFree = true;
        }


        //actual deallocation cases
        char* move = (char*) temp;
        // coalesce both sides
        if(leftFree && rightFree) {
            // 4 sentinels saved = + 16
            int newBlockSize = *left + (**currentBlockIter * -1) + *right + 16;
            *left = newBlockSize;
            move = (char*) left;
            move += newBlockSize + 4;
            int* sent2 = (int*) move;
            *sent2 = newBlockSize;
        }
        // coalesce right side
        else if(!leftFree && rightFree) {
            int newBlockSize = (**currentBlockIter * -1) + *right + 8;
            **currentBlockIter = newBlockSize;
            move = (char*) *currentBlockIter;
            move += newBlockSize + 4;
            int* sent2 = (int*) move;
            *sent2 = newBlockSize;
        }
        // coalesce left side
        else if(leftFree && !rightFree) {
            int newBlockSize = (**currentBlockIter * -1) + *left + 8;
            *left = newBlockSize;
            move = (char*) left;
            move += newBlockSize + 4;
            int* sent2 = (int*) move;
            *sent2 = newBlockSize;
        }
        // no coalescing needed
        else {
            **currentBlockIter *= -1;
            move = (char*) *currentBlockIter;
            move += **currentBlockIter + 4;
            int* sent2 = (int*) move;
            *sent2 *= -1;
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }


    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
