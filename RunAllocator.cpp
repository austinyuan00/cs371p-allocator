// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <string>

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */
    int tests;
    cin >> tests >> ws;
    string line;

    while(tests > 0) {
        my_allocator<double, 1000> a;

        getline(cin,line);

        cout.flush();

        while(line.length() > 0) {
            int inp = stoi(line);

            if(inp > 0) {
                a.allocate(inp);
            }
            // free inp th busy block
            else {
                //find the busy block
                my_allocator<double, 1000>::iterator b = a.begin();
                my_allocator<double, 1000>::iterator e = a.end();
                int count = 0;
                while(b != e) {
                    if(**b < 0)
                        count++;
                    if(count == (-1 * inp))
                        break;
                    ++b;
                }
                double* input2 = (double*) *b;
                a.deallocate(input2, sizeof(*input2));

            }
            getline(cin,line);
        }
        a.print_blocks();
        --tests;
    }
    return 0;
}
