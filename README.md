# CS371p: Object-Oriented Programming Allocator Repo

* Name: Austin Yuan

* EID: ahy259

* GitLab ID: austinyuan00

* HackerRank ID: austinyuan00

* Git SHA: dad41df65764514afb73d19438b28fda31da9813

* GitLab Pipelines: https://gitlab.com/austinyuan00/cs371p-allocator/-/pipelines

* Estimated completion time: 10

* Actual completion time: 15

* Comments: passing all hackerrank and public test repo tests, but could never get the given unit tests to work
